﻿using DomainLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLayer.Services;

namespace OnionArchitectureProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet(nameof(GetUser))]
        public IActionResult GetUser(int id)
        {
            var result = _userService.GetUser(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("No records found");

        }
        [HttpGet(nameof(GetAllUsers))]
        public IActionResult GetAllUsers()
        {
            var result = _userService.GetAllUsers();
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("No records found");

        }
        [HttpPost(nameof(InsertUser))]
        public IActionResult InsertUser(User user)
        {
            _userService.InsertUser(user);
            return Ok("Data inserted");

        }
        [HttpPut(nameof(UpdateUser))]
        public IActionResult UpdateUser(User user)
        {
            _userService.UpdateUser(user);
            return Ok("Updation done");

        }
        [HttpDelete(nameof(DeleteUser))]
        public IActionResult DeleteUser(int Id)
        {
            _userService.DeleteUser(Id);
            return Ok("Data Deleted");

        }
    }
}
