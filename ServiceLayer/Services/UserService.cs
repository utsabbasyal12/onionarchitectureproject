﻿using DomainLayer.Models;
using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public class UserService : IUserService
    {
        private IRepository<User> _repository;

        public UserService(IRepository<User> repository)
        {
            _repository = repository;
        }
        public IEnumerable<User> GetAllUsers()
        {
            return _repository.GetAll();
        }

        public User GetUser(int id)
        {
            return _repository.Get(id);
        }

        public void InsertUser(User user)
        {
            _repository.Insert(user);
        }
        public void UpdateUser(User user)
        {
            _repository.Update(user);
        }

        public void DeleteUser(int id)
        {
            User user = GetUser(id);
            _repository.Remove(user);
            _repository.SaveChanges();
        }

    }
}
